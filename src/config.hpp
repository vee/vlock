/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * vlock, a very simple screen locker for Windows
 * Copyright (c) 2023 Vendicated
 */

#pragma once
#include <Windows.h>

#include <string>

class InvalidConfigException : public std::exception {
   public:
    int lineNum;
    std::string message;

    InvalidConfigException(int lineNum, std::string message)
        : lineNum(lineNum), message(message) {}

    std::string what();
};

struct Config {
    std::wstring Password;
    bool RequireEnter;
    bool ClearCountsAsFail;

    bool ShowBackground;
    COLORREF BackgroundColor;
    std::string BackgroundImage;

    bool ShowFail;
    COLORREF FailColor;
    std::string FailImage;
    bool DisableTaskManager;
};

extern struct Config config;

bool ParseConfig();
