/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * vlock, a very simple screen locker for Windows
 * Copyright (c) 2023 Vendicated
 */

#pragma once

#include <Windows.h>

#ifdef __cplusplus
#define DLL_EXPORT extern "C" __declspec(dllexport)
#else
#define DLL_EXPORT
#include <stdbool.h>
#endif

struct KHConfig {
    bool ClearCountsAsFail;
    bool RequireEnter;
    int PwLength;
    const wchar_t* Pw;
    void (*OnFail)(const wchar_t* pw);
    void (*OnSuccess)();
    void (*OnInput)(wchar_t inputBuf[], int inputBufLength,
                    const wchar_t* fullInput);
};

DLL_EXPORT bool InstallKHook();
DLL_EXPORT bool UninstallKHook();
DLL_EXPORT bool IsKHookInstalled();
DLL_EXPORT void SetKHookConfig(struct KHConfig config);
