#include "util.hpp"

#include <fstream>

std::wstring stringToWstring(const std::string &string) {
    if (string.empty()) {
        return L"";
    }

    const auto size_needed = MultiByteToWideChar(
        CP_UTF8, 0, &string.at(0), (int)string.size(), nullptr, 0);
    if (size_needed <= 0) {
        throw std::runtime_error("MultiByteToWideChar() failed: " +
                                 std::to_string(size_needed));
    }

    std::wstring result(size_needed, 0);
    MultiByteToWideChar(CP_UTF8, 0, &string.at(0), (int)string.size(),
                        &result.at(0), size_needed);
    return result;
}
