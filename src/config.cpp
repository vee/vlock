/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * vlock, a very simple screen locker for Windows
 * Copyright (c) 2023 Vendicated
 */

#include "config.hpp"

#include <Shlobj.h>

#include <filesystem>
#include <fstream>
#include <stdexcept>
#include <string>

#include "default_config.h"
#include "util.hpp"

std::string CONFIG_FILE_NAME = "vlock.conf";
std::string CONFIG_FILE;

std::string InvalidConfigException::what() {
    return "Invalid config file " + CONFIG_FILE + ".\n\non line " +
           std::to_string(lineNum) + ": " + message;
}

Config config;

DWORD parseKey(char c, int lineNum) {
    if (c >= '0' && c <= '9') {
        return 0x30 + c - '0';
    }

    char l = tolower(c);
    if (l >= 'a' && l <= 'z') {
        return 0x41 + l - 'a';
    }

    throw InvalidConfigException(lineNum, "Invalid key " + c);
}

DWORD parseKey(std::string raw, int lineNum) {
    if (raw.length() == 1) {
        return parseKey(raw[0], lineNum);
    } else if (raw.rfind("0x", 0) != 0) {
        throw InvalidConfigException(lineNum, "Invalid key " + raw);
    }

    try {
        return std::stol(raw.substr(2), nullptr, 16);
    } catch (std::exception) {
        throw InvalidConfigException(lineNum, "Invalid key " + raw);
    }
}

int parseNum(std::string raw, int lineNum) {
    try {
        return std::stoi(raw);
    } catch (std::exception) {
        throw InvalidConfigException(lineNum, "Invalid number " + raw);
    }
}

COLORREF parseColor(std::string hex, int lineNum) {
    if (hex.length() != 7 || hex.rfind("#", 0) != 0) {
        throw InvalidConfigException(lineNum, "Invalid color " + hex);
    }

    try {
        long i = std::stol(hex.substr(1), nullptr, 16);

        return RGB(i >> 16 & 0xff, i >> 8 & 0xff, i & 0xff);
    } catch (std::exception) {
        throw InvalidConfigException(lineNum, "Invalid color " + hex);
    }
}

bool parseBool(std::string raw, int lineNum) {
    toLower(raw);
    if (raw == "true" || raw == "1" || raw == "yes" || raw == "y")
        return true;
    else if (raw == "false" || raw == "0" || raw == "no" || raw == "n")
        return false;
    else
        throw InvalidConfigException(lineNum, "Invalid boolean value " + raw);
}

std::string parseFile(int lineNum, std::string path) {
    if (!std::filesystem::exists(path))
        throw InvalidConfigException(lineNum,
                                     "File '" + path + "' does not exist.");
    return path;
}

void initDefaults() {
    config.Password = L"Password";
    config.RequireEnter = true;
    config.ClearCountsAsFail = false;

    config.ShowBackground = true;
    config.BackgroundColor = RGB(0, 0, 0);
    config.BackgroundImage = "";

    config.ShowFail = true;
    config.FailColor = RGB(0xff, 0, 0);
    config.FailImage = "";

    config.DisableTaskManager = true;
}

void writeDefaultConfig(std::filesystem::path p) {
    std::ofstream file(p);
    file.write(DEFAULT_CONFIG, strlen(DEFAULT_CONFIG));
}

void parseConfig() {
    initDefaults();

    PWSTR appdata = NULL;
    if (SHGetKnownFolderPath(FOLDERID_RoamingAppData, KF_FLAG_CREATE, NULL,
                             &appdata) == S_OK) {
        std::filesystem::path p = appdata;
        p.append(CONFIG_FILE_NAME);

        std::ifstream file(p);
        if (!file.good()) {
            writeDefaultConfig(p);
            return;
        }

        CONFIG_FILE = p.string();

        int lineNum = 1;
        for (std::string line; getline(file, line); lineNum++) {
            trim(line);
            if (line.length() == 0 || line.rfind("#", 0) == 0) {
                continue;
            }

            size_t pos = line.find("=");
            if (pos == std::string::npos) {
                throw InvalidConfigException(
                    lineNum, "Invalid line '" + line + "': Missing =");
            }

            std::string key = line.substr(0, pos);
            std::string value = line.substr(pos + 1);
            rtrim(key);
            ltrim(value);

            if (key == "Password")
                config.Password = stringToWstring(value);
            else if (key == "RequireEnter")
                config.RequireEnter = parseBool(value, lineNum);
            else if (key == "ClearCountsAsFail")
                config.ClearCountsAsFail = parseBool(value, lineNum);
            else if (key == "ShowBackground")
                config.ShowBackground = parseBool(value, lineNum);
            else if (key == "BackgroundColor")
                config.BackgroundColor = parseColor(value, lineNum);
            else if (key == "BackgroundImage")
                config.BackgroundImage = parseFile(lineNum, value);
            else if (key == "FailImage")
                config.FailImage = parseFile(lineNum, value);
            else if (key == "ShowFail")
                config.ShowFail = parseBool(value, lineNum);
            else if (key == "FailColor")
                config.FailColor = parseColor(value, lineNum);
            else if (key == "DisableTaskManager")
                config.DisableTaskManager = parseBool(value, lineNum);
            else
                throw InvalidConfigException(lineNum,
                                             "Unrecognised key: " + line);
        }

        file.close();
    }
    CoTaskMemFree(appdata);
}

bool ParseConfig() {
    std::string msg;
    try {
        parseConfig();
        return true;
    } catch (InvalidConfigException &e) {
        msg = e.what();
    } catch (std::exception &e) {
        msg = "An unknown error occurred while parsing the config file:\n\n" +
              std::string(e.what());
    }

    UINT msgBoxType = MB_OK | MB_ICONERROR;
    if (!CONFIG_FILE.empty()) {
        msg += "\n\nEdit it now?";
        msgBoxType = MB_YESNO | MB_ICONERROR;
    }

    int choice =
        MessageBoxW(NULL, stringToWstring(msg).c_str(), L"vlock", msgBoxType);

    if (choice == IDYES) {
        ShellExecuteW(NULL, L"open", stringToWstring(CONFIG_FILE).c_str(), NULL,
                      NULL, SW_SHOWDEFAULT);
    }

    return false;
}
