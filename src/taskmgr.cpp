/*
 * SPDX-License-Identifier: GPL-3.0-or-later
 * vlock, a very simple screen locker for Windows
 * Copyright (c) 2023 Vendicated
 */

#include "taskmgr.h"

#include <Windows.h>
#include <winreg.h>

bool SetTskManVal(DWORD value) {
    HKEY key;

    auto res = RegOpenKeyExW(
        HKEY_CURRENT_USER,
        L"Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System\\", 0,
        KEY_ALL_ACCESS, &key);

    if (res != ERROR_SUCCESS) return false;

    res = RegSetValueExW(key, L"DisableTaskmgr", 0, REG_DWORD, (LPBYTE)&value,
                         sizeof(DWORD));

    RegCloseKey(key);
    return res == ERROR_SUCCESS;
}

bool DisableTskMan() { return SetTskManVal(1); }

bool RestoreTskMan() { return SetTskManVal(0); }
